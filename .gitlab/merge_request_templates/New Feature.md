# New Feature

> Describe what kind of feature that is going to be merged into the master branch.

# Pre-Merge Checklist

- [ ] Do code review
- [ ] Ensure all tests pass (i.e. green)
- [ ] New feature works when deployed on simulator

# Related Issues

> Mention identifier of any issues that are relevant to this merge request. You
> can prepend the issue identifier with **Close** or **Finish** to automatically
> close the issue in the Issue Tracker.

> Example: `Related to ♯1, ♯2. Close ♯1` means this merge request related to issue
> number 1 and 2, and close issue number 1 as well. Remember to use ASCII's `#`!

# Time Estimate

> Put time estimate in GitLab-supported format: `/estimate NUM{w,d,h,m}`, e.g. `/estimate 10h`

> If you are not sure how to estimate the duration, try using the following
> heuristics:
>  - Weight 1 == 2 hours
>  - Weight 2 == 4 hours
>  - Weight 3 == 6 hours
>  - Weight 5 == 10 hours
