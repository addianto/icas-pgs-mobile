# User Story/Feature Request

> Story name format: `As a <TYPE OF USER>, I want <SOME GOAL>, so that <SOME REASON>.`

# Tasks

> Write down tasks for the developer.

- [ ] Create new feature branch
- [ ] Implement the feature in the new branch
- [ ] Submit merge request when ready to be integrated into main branch

# Acceptance Criteria

> Define criteria that determine whether the development of this feature is
> completed. You can use the following template:
>`Given <SOME PRECONDITION>, when user do <SOME ACTION>, then user expect <SOME RESULT>`

# Related Features

> Mention identifier of issues with **Feature** label that are related to this
> feature. Features can relate with each other if the implementation overlap,
> e.g. the code are written in the same module(s). If you cannot determine
> existing features, just leave this section empty.

# Time Estimate

> Put time estimate in GitLab-supported format: `/estimate NUM{w,d,h,m}`, e.g. `/estimate 10h`

> If you are not sure how to estimate the duration, try using the following
> heuristics:
>  - Weight 1 == 2 hours
>  - Weight 2 == 4 hours
>  - Weight 3 == 6 hours
>  - Weight 5 == 10 hours
