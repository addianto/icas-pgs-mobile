# Bug/Error Report

Please write your report by answering the following questions:

> - Describe the error or fault that you experienced in the app
> - What device did you use?
> - When the bug/error occur?
> - Is this bug/error related to any existing features? If so,
>   please mention the ID numbers (in the issue tracker)
> - If possible, describe solutions to fix this bug
