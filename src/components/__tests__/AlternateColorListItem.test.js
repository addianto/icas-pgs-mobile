'use strict'
import 'react-native'
import React from 'react'
import {
  Text
} from 'native-base'
import AlternateColorListItem from '../AlternateColorListItem'
import renderer from 'react-test-renderer'

describe('an alternating color list item', () => {
  describe('given required props', () => {
    it('renders correctly', () => {
      let component = renderer.create(
        <AlternateColorListItem id={0} />
      )
      assertMatchSnapshot(component)
    })

    it('renders correctly with single child', () => {
      let component = renderer.create(
        <AlternateColorListItem id={0}>
          <Text>A quick brown fox</Text>
        </AlternateColorListItem>
      )
      assertMatchSnapshot(component)
    })

    it('renders correctly with multiple children', () => {
      let component = renderer.create(
        <AlternateColorListItem id={0}>
          <Text>A quick brown fox</Text>
          <Text>Jumps over a</Text>
          <Text>Lazy fox</Text>
        </AlternateColorListItem>
      )
      assertMatchSnapshot(component)
    })
  })

  describe('given required & optional props', () => {
    it('renders correctly', () => {
      let component = renderer.create(
        <AlternateColorListItem
          id={0}
          oddColor={colors.firstColor}
          evenColor={colors.secondColor} />
      )
      assertMatchSnapshot(component)
    })

    it('renders correctly with single child', () => {
      let component = renderer.create(
        <AlternateColorListItem
          id={0}
          oddColor={colors.firstColor}
          evenColor={colors.secondColor}>
          <Text>A quick brown fox</Text>
        </AlternateColorListItem>
      )
      assertMatchSnapshot(component)
    })

    it('renders correctly with multiple children', () => {
      let component = renderer.create(
        <AlternateColorListItem
          id={0}
          oddColor={colors.firstColor}
          evenColor={colors.secondColor}>
          <Text>A quick brown fox</Text>
          <Text>Jumps over a</Text>
          <Text>Lazy fox</Text>
        </AlternateColorListItem>
      )
      assertMatchSnapshot(component)
    })
  })
})

function assertMatchSnapshot (component) {
  expect(component.toJSON()).toMatchSnapshot()
}

const colors = {
  'firstColor': '#ff0000',
  'secondColor': '#0000ff'
}
