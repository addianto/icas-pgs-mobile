'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  ListItem
} from 'native-base'
import { colors } from '../styles'

export default class AlternateColorListItem extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element)
    ]),
    oddColor: PropTypes.string,
    evenColor: PropTypes.string
  }

  static defaultProps = {
    oddColor: colors.white,
    evenColor: colors.lavender
  }

  render () {
    const { id, children, oddColor, evenColor } = this.props
    const rowColor = (id % 2 !== 0) ? oddColor : evenColor

    return (
      <ListItem style={[styles.item, {backgroundColor: rowColor}]}>
        {children}
      </ListItem>
    )
  }
}

const styles = StyleSheet.create({
  item: {
    marginLeft: -15,
    paddingLeft: 15
  }
})
