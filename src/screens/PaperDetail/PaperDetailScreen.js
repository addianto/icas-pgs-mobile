'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Linking,
  StyleSheet
} from 'react-native'
import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Content,
  H2,
  Header,
  Icon,
  Left,
  Text,
  Title
} from 'native-base'
import { templates } from '../../styles'

export default class PaperDetailScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack } = this.props.navigation
    const { abstract, authors, room, startTime, endTime,
      title, downloadUrl } = this.props.navigation.state.params

    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Paper</Title>
          </Body>
        </Header>
        <Content>
          <Card>
            <CardItem header>
              <H2>{title}</H2>
            </CardItem>
            <CardItem>
              <Text style={styles.authors}>{authors}</Text>
            </CardItem>
            <CardItem>
              <Text note>{room} : {startTime} - {endTime}</Text>
            </CardItem>
            <CardItem body>
              <Text>{abstract}</Text>
            </CardItem>
            {downloadUrl.length > 0 &&
            <CardItem footer>
              <Button iconLeft bordered
                onPress={() => this._downloadPaper(downloadUrl)}>
                <Icon name='download' />
                <Text>Download Paper</Text>
              </Button>
            </CardItem>
            }
          </Card>
        </Content>
      </Container>
    )
  }

  _downloadPaper (url) {
    console.log(`[Debug] Param URL: ${url}`)

    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log(`Can't handle url: ${url}`)
      } else {
        return Linking.openURL(url)
      }
    }).catch(err => console.error('An error occured', err))
  }
}

const styles = StyleSheet.create({
  authors: {
    fontStyle: 'italic'
  }
})
