'use strict'
import 'react-native'
import React from 'react'
import PaperDetailScreen from '../PaperDetailScreen'
import renderer from 'react-test-renderer'

describe('a paper detail screen', () => {
  let mockState = null
  let mockNavigation = null

  beforeEach(() => {
    mockState = {
      params: {
        abstract: 'Lorem ipsum dolor sit amet',
        authors: 'John Doe, Jane Doe',
        room: 'Room A',
        startTime: '12:00',
        endTime: '13:00',
        title: 'A Paper About Lorem Ipsum',
        downloadUrl: ''
      }
    }

    mockNavigation = {
      goBack: jest.fn(),
      state: mockState
    }
  })

  it('renders correctly given required props', () => {
    const rendered = renderer.create(
      <PaperDetailScreen navigation={mockNavigation} />
    ).toJSON()

    expect(rendered).toMatchSnapshot()
  })

  it('renders correctly given non-empty download URL', () => {
    mockState.params.downloadUrl = 'http://example.com/example.pdf'
    const rendered = renderer.create(
      <PaperDetailScreen navigation={mockNavigation} />
    ).toJSON()

    expect(rendered).toMatchSnapshot()
  })
})
