'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  ListView,
  StyleSheet
} from 'react-native'
import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Content,
  Header,
  Icon,
  Left,
  Spinner,
  Text,
  Title
} from 'native-base'
import { colors, templates } from '../../styles'

const API_URL = 'https://api.rss2json.com/v1/api.json'
const RSS_URL = 'https://icas-pgs.ui.ac.id/category/news/feed/'

function createFetchURL () {
  let fetchURL = `${API_URL}?rss_url=${RSS_URL}`

  return fetchURL
}

export default class NewsScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)

    this.state = {
      isLoading: true
    }
  }

  componentDidMount () {
    let fetchURL = createFetchURL()
    return fetch(fetchURL)
      .then(response => response.json())
      .then(responseJson => {
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.setState({
          isLoading: false,
          dataSource: ds.cloneWithRows(responseJson.items)
        }, () => { /* NOTHING */ })
      })
  }

  render () {
    const { goBack, navigate } = this.props.navigation

    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>News</Title>
          </Body>
        </Header>
        <Content>
          {this.state.isLoading ? (
            <Spinner color={colors.indigo} />
          ) : (
            <ListView dataSource={this.state.dataSource}
              renderRow={item =>
                <Card>
                  <CardItem header>
                    <Text style={styles.title}>{item.title}</Text>
                  </CardItem>
                  <CardItem>
                    <Text>{item.description}</Text>
                  </CardItem>
                  <CardItem footer>
                    <Left>
                      <Button small transparent
                        onPress={() => navigate('NewsContent',
                          {title: `${item.title}`, content: `${item.content}`}
                        )}>
                        <Text>Read more</Text>
                      </Button>
                    </Left>
                  </CardItem>
                </Card>
              }
            />
          )}
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  }
})
