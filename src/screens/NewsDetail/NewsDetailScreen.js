'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  StyleSheet,
  WebView
} from 'react-native'
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Icon,
  Left,
  Title
} from 'native-base'
import { templates } from '../../styles'

export default class NewsDetailScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack, state } = this.props.navigation
    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{state.params.title}</Title>
          </Body>
        </Header>
        <Content contentContainerStyle={styles.content}>
          <WebView source={{body: `${state.params.content}`}, // eslint-disable-line
            {html: `${state.params.content}`}} />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1
  }
})
