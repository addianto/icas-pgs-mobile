'use strict'
import React from 'react'
import MainMenuScreen from '../MainMenuScreen'
import renderer from 'react-test-renderer'

describe('a main menu screen', () => {
  let mockNavigation = null
  let component = null

  beforeEach(() => {
    mockNavigation = {
      navigate: jest.fn(screen => { return screen })
    }

    component = renderer.create(
      <MainMenuScreen navigation={mockNavigation} />
    )
  })

  it('renders correctly', () => {
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})
