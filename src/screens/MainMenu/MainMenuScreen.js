'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Image,
  Linking,
  StyleSheet
} from 'react-native'
import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Content,
  Header,
  Icon,
  Left,
  Right,
  Text,
  Thumbnail,
  Title
} from 'native-base'
import { colors, templates } from '../../styles'

export default class MainMenuScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { navigate } = this.props.navigation

    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Thumbnail small
              square source={require('../../assets/ic_makara_fia.png')} />
          </Left>
          <Body>
            <Title>ICASPGS Depok 2017</Title>
          </Body>
          <Right>
            <Button iconRight transparent light
              onPress={() => this._downloadProgramBook()}>
              <Icon name='download' />
            </Button>
          </Right>
        </Header>
        <Content>
          <Card>
            <CardItem>
              <Image
                resizeMode='contain'
                source={require('../../assets/conf_logo.png')}
                style={styles.logo} />
            </CardItem>
          </Card>
          <Button block style={styles.menu}
            onPress={() => navigate('News')}>
            <Text>News</Text>
          </Button>
          <Button block style={styles.menu}
            onPress={() => navigate('Schedules')}>
            <Text>Event Rundown</Text>
          </Button>
          <Button block style={styles.menu}
            onPress={() => navigate('Plenary')}>
            <Text>Plenary Sessions</Text>
          </Button>
          <Button block style={styles.menu}
            onPress={() => navigate('VenueMap')}>
            <Text>Venue Map</Text>
          </Button>
          <Button block style={styles.menu}
            onPress={() => navigate('Streams')}>
            <Text>Parallel Sessions</Text>
          </Button>
          <Button block style={styles.menu}
            onPress={() => navigate('Contact')}>
            <Text>Contact</Text>
          </Button>
          <Button block style={styles.menu}
            onPress={() => navigate('Legal')}>
            <Text>Legal Notice</Text>
          </Button>
        </Content>
        <Button full style={styles.footer}>
          <Text note style={styles.copyright}>
            Copyright (c) 2017 Faculty of Administrative Science Universitas Indonesia
          </Text>
        </Button>
      </Container>
    )
  }

  _downloadProgramBook () {
    const url = 'https://icas-pgs.ui.ac.id/wp-content/uploads/sites/40/2017/10/Program-Book.pdf'

    console.log(`[Debug] Param URL: ${url}`)

    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log(`Can't handle url: ${url}`)
      } else {
        return Linking.openURL(url)
      }
    }).catch(err => console.error('An error occured', err))
  }
}

const styles = StyleSheet.create({
  footer: {
    backgroundColor: colors.indigo,
    marginTop: 10
  },
  copyright: {
    color: colors.white,
    fontSize: 10,
    textAlign: 'center'
  },
  menu: {
    margin: 2
  },
  logo: {
    flex: 1,
    height: 100,
    width: null
  }
})
