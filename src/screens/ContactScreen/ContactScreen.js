'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Content,
  H1,
  Header,
  Icon,
  Left,
  Text,
  Title
} from 'native-base'
import { templates } from '../../styles'

export default class ContactScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack } = this.props.navigation

    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Contact Us</Title>
          </Body>
        </Header>
        <Content>
          <Card>
            <CardItem header>
              <Body>
                <H1>Organizer</H1>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>The 1st ICAS-PGS is organized by the Faculty of Administrative Science Universitas Indonesia.</Text>
                <Text>Our mail address is as follows:</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>M Building 2nd Floor, Faculty of Social and Political Sciences Universitas Indonesia</Text>
                <Text>UI Depok Campus 16424, Indonesia</Text>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem header>
              <Body>
                <H1>Contact Information</H1>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>If you have any questions or requests, please contact us at: icas-pgs@ui.ac.id</Text>
                <Text>Or call us directly: +62178849145</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    )
  }
}
