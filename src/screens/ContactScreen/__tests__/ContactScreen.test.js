'use strict'
import React from 'react'
import ContactScreen from '../ContactScreen'
import renderer from 'react-test-renderer'

describe('a contact screen', () => {
  let rendered = null
  let mockNavigation = null

  beforeEach(() => {
    mockNavigation = {
      goBack: jest.fn()
    }

    rendered = renderer.create(<ContactScreen navigation={mockNavigation} />)
  })

  it('renders correctly', () => {
    expect(rendered.toJSON()).toMatchSnapshot()
  })
})
