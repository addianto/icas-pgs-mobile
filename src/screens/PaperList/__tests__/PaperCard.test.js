'use strict'
import React from 'react'
import PaperCard from '../PaperCard'
import renderer from 'react-test-renderer'

describe('a paper card', () => {
  it('renders correctly', () => {
    const mockNavigate = jest.fn()
    const rendered = renderer.create(
      <PaperCard
        authors='Lorem Ipsum, Dolor Sit, Amet'
        title='A Paper About Lorem Ipsum'
        abstract='A quick brown fox jumps over a lazy fox'
        startTime='08:10'
        endTime='09:10'
        room='Room A'
        navigate={mockNavigate}
      />
    ).toJSON()

    expect(rendered).toMatchSnapshot()
  })
})
