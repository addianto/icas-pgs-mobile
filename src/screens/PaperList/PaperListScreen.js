'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  FlatList
} from 'react-native'
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  Icon,
  Title
} from 'native-base'
import PaperCard from './PaperCard'
import PaperData from '../../assets/papers.json'
import { getPapersByParallelSession } from '../../util'
import { templates } from '../../styles'

export default class PaperListScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack, navigate } = this.props.navigation
    const { params } = this.props.navigation.state

    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Papers</Title>
          </Body>
        </Header>
        <Content>
          <FlatList
            data={this._getPapersData(PaperData, params.parallelSession, navigate)}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderPaperCard}
          />
        </Content>
      </Container>
    )
  }

  _getPapersData (data, session, navigate) {
    const papers = getPapersByParallelSession(data, session)

    papers.forEach(paper => {
      paper['navigate'] = navigate
    })

    return papers
  }

  _keyExtractor (item, index) {
    return item.id
  }

  _renderPaperCard (paper) {
    return (
      <PaperCard
        abstract={paper.item.abstract}
        authors={paper.item.authors}
        startTime={paper.item.startTime}
        endTime={paper.item.endTime}
        room={paper.item.room}
        title={paper.item.title}
        navigate={paper.item.navigate}
        downloadUrl={paper.item.downloadUrl} />
    )
  }
}
