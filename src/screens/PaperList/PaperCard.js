'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  Body,
  Button,
  Card,
  CardItem,
  H2,
  Icon,
  Left,
  Right,
  Text
} from 'native-base'

export default class PaperCard extends Component {
  static propTypes = {
    abstract: PropTypes.string.isRequired,
    authors: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
    navigate: PropTypes.func.isRequired,
    room: PropTypes.string.isRequired,
    startTime: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    downloadUrl: PropTypes.string
  }

  render () {
    const { abstract, authors, endTime, navigate, room, startTime,
      title, downloadUrl } = this.props

    return (
      <Card>
        <CardItem header>
          <H2>{title}</H2>
        </CardItem>
        <CardItem>
          <Body>
            <Text style={styles.authors}>{authors}</Text>
          </Body>
        </CardItem>
        <CardItem footer style={styles.footer}>
          <Left style={styles.room}>
            <Icon name='locate' />
            <Text>{room}</Text>
          </Left>
          <Body style={styles.time}>
            <Icon name='time' />
            <Text>{startTime} - {endTime}</Text>
          </Body>
          <Right style={styles.abstract}>
            <Button transparent
              onPress={() => navigate('Paper', {
                abstract: abstract,
                authors: authors,
                endTime: endTime,
                room: room,
                startTime: startTime,
                title: title,
                downloadUrl: downloadUrl
              })}>
              <Text>View Abstract</Text>
            </Button>
          </Right>
        </CardItem>
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  authors: {
    fontStyle: 'italic'
  },
  footer: {
    paddingHorizontal: 10
  },
  room: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  time: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  abstract: {
    alignItems: 'center',
    flex: 1.25,
    flexDirection: 'column',
    flexWrap: 'wrap'
  }
})
