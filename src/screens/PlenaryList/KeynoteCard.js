'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Body,
  Card,
  CardItem,
  Left,
  Text
} from 'native-base'

export default class KeynoteCard extends Component {
  static propTypes = {
    speakerName: PropTypes.string.isRequired,
    speakerTitle: PropTypes.string.isRequired,
    speechTitle: PropTypes.string.isRequired,
    speechAbstract: PropTypes.string,
    children: PropTypes.node
  }

  render () {
    const { speakerName, speakerTitle, speechTitle, speechAbstract,
      children } = this.props

    return (
      <Card>
        <CardItem header>
          <Left>
            {children}
            <Body>
              <Text>{speakerName}</Text>
              <Text note>{speakerTitle}</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem cardBody>
          <Body>
            <Text>{speechTitle}</Text>
            <Text note>{speechAbstract}</Text>
          </Body>
        </CardItem>
      </Card>
    )
  }
}
