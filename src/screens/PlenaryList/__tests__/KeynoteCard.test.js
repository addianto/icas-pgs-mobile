'use strict'
import 'react-native'
import React from 'react'
import KeynoteCard from '../KeynoteCard'
import renderer from 'react-test-renderer'

describe('a keynote card', () => {
  it('renders correctly with all required props', () => {
    const component = renderer.create(
      <KeynoteCard
        speakerName='John Doe'
        speakerTitle='Plain Old Citizen'
        speechTitle='Lorem Ipsum Dolor Sit Amet' />
    )
    expect(component.toJSON()).toMatchSnapshot()
  })

  it('renders correctly with all required and some optional props', () => {
    const component = renderer.create(
      <KeynoteCard
        speakerName='Jane Doe'
        speakerTitle='Yet Another Plain Old Citizen'
        speechTitle='Lorem Ipsum Dolor Sit Amet'
        speechAbstract='Something something something'>
      </KeynoteCard>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
})
