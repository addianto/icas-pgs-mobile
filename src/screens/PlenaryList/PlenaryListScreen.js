'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Body,
  Button,
  Container,
  Header,
  Icon,
  Left,
  Tab,
  Tabs,
  Thumbnail,
  Title
} from 'native-base'
import KeynoteCard from './KeynoteCard'
import DayOneKeynote from '../../assets/keynotes_30-10-2017.json'
import DayTwoKeynote from '../../assets/keynotes_31-10-2017.json'
import { templates } from '../../styles'

export default class PlenaryListScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack } = this.props.navigation

    return (
      <Container>
        <Header hasTabs style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Plenary Sessions</Title>
          </Body>
        </Header>
        <Tabs initialPage={0}>
          <Tab heading='Day 1'>
            <KeynoteCard
              speakerName={DayOneKeynote[0].speaker.name}
              speakerTitle={DayOneKeynote[0].speaker.title}
              speechTitle={DayOneKeynote[0].speech.title}>
              <Thumbnail source={require('../../assets/kn_speaker_day_1_1.jpg')} />
            </KeynoteCard>
            <KeynoteCard
              speakerName={DayOneKeynote[1].speaker.name}
              speakerTitle={DayOneKeynote[1].speaker.title}
              speechTitle={DayOneKeynote[1].speech.title}>
              <Thumbnail source={require('../../assets/kn_speaker_day_1_2.jpg')} />
            </KeynoteCard>
          </Tab>
          <Tab heading='Day 2'>
            <KeynoteCard
              speakerName={DayTwoKeynote[0].speaker.name}
              speakerTitle={DayTwoKeynote[0].speaker.title}
              speechTitle={DayTwoKeynote[0].speech.title}>
              <Thumbnail source={require('../../assets/kn_speaker_day_2_1.jpg')} />
            </KeynoteCard>
            <KeynoteCard
              speakerName={DayTwoKeynote[1].speaker.name}
              speakerTitle={DayTwoKeynote[1].speaker.title}
              speechTitle={DayTwoKeynote[1].speech.title}>
              <Thumbnail source={require('../../assets/kn_speaker_day_2_2.jpg')} />
            </KeynoteCard>
          </Tab>
        </Tabs>
      </Container>
    )
  }
}
