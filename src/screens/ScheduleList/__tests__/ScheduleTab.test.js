'use strict'
import React from 'react'
import ScheduleTab from '../ScheduleTab'
import renderer from 'react-test-renderer'

describe('a schedule tab', () => {
  const sampleHeading = 'A Tab'
  const sampleData = [
    {
      start: '10:00',
      end: '11:00',
      title: 'Event A',
      location: 'Convention Hall'
    },
    {
      start: '11:00',
      end: '12:00',
      title: 'Event B',
      location: 'Room 1'
    },
    {
      start: '12:00',
      end: '13:00',
      title: 'Lunch',
      location: 'Dining Hall'
    }
  ]

  it('renders correctly when given required props', () => {
    const noLocationData = []

    sampleData.forEach(item => {
      const noLocation = {
        start: item.start,
        end: item.end,
        title: item.title
      }

      noLocationData.push(noLocation)
    })

    const component = renderer.create(
      <ScheduleTab heading={sampleHeading}
        schedules={noLocationData} />
    )

    expect(component.toJSON()).toMatchSnapshot()
  })

  it('renders correctly when given required & optional props', () => {
    const component = renderer.create(
      <ScheduleTab heading={sampleHeading}
        schedules={sampleData} />
    )

    expect(component.toJSON()).toMatchSnapshot()
  })
})
