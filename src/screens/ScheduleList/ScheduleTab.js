'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Body,
  List,
  Left,
  Right,
  Tab,
  Text
} from 'native-base'
import AlternateColorListItem from '../../components/AlternateColorListItem'

export default class ScheduleTab extends Component {
  static propTypes = {
    heading: PropTypes.string.isRequired,
    schedules: PropTypes.arrayOf(PropTypes.object).isRequired
  }

  render () {
    const { heading, schedules } = this.props

    return (
      <Tab heading={heading}>
        <List
          dataArray={this._prepareData(schedules)}
          keyExtractor={this._extractKey}
          renderRow={this._renderListItem}
        />
      </Tab>
    )
  }

  _prepareData (data) {
    const schedules = []
    let id = 0

    data.forEach(schedule => {
      schedule['id'] = ++id
      schedules.push(schedule)
    })

    return schedules
  }

  _extractKey (item, index) {
    return item.id
  }

  _renderListItem (schedule) {
    const isLocationProvided = schedule.location !== ''

    return (
      <AlternateColorListItem id={schedule.id}>
        <Left>
          <Text note>{schedule.start} - {schedule.end}</Text>
        </Left>
        <Body>
          <Text>{schedule.title}</Text>
          {isLocationProvided && <Text note>{schedule.location}</Text>}
        </Body>
        <Right />
      </AlternateColorListItem>
    )
  }
}
