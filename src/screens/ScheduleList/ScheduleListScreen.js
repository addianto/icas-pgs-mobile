'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Body,
  Button,
  Container,
  Header,
  Icon,
  Left,
  Tabs,
  Title
} from 'native-base'
import ScheduleTab from './ScheduleTab'
import DayOneSchedule from '../../assets/day1_schedules.json'
import DayTwoSchedule from '../../assets/day2_schedules.json'
import { templates } from '../../styles'

export default class ScheduleListScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack } = this.props.navigation

    return (
      <Container>
        <Header hasTabs style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Event Rundown</Title>
          </Body>
        </Header>
        <Tabs initialPage={0}>
          <ScheduleTab
            heading='Day 1'
            schedules={DayOneSchedule} />
          <ScheduleTab
            heading='Day 2'
            schedules={DayTwoSchedule} />
        </Tabs>
      </Container>
    )
  }
}
