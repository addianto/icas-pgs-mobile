'use strict'
import 'react-native'
import React from 'react'
import SplashScreen from '../SplashScreen'
import renderer from 'react-test-renderer'

jest.useFakeTimers()

describe('splash screen', () => {
  let mockNavigation = null
  let tree = null

  beforeEach(() => {
    mockNavigation = {
      navigate: jest.fn(screen => { return screen })
    }

    const component = renderer.create(
      <SplashScreen navigation={mockNavigation} />
    )
    tree = component.toJSON()
  })

  it('shows loading animation', () => {
    expect(mockNavigation.navigate).not.toBeCalled()
    expect(tree).toMatchSnapshot()
  })

  it('changes to main menu after three seconds', () => {
    jest.runAllTimers()
    expect(mockNavigation.navigate).toHaveBeenCalledTimes(1)
    expect(mockNavigation.navigate).toHaveBeenCalledWith('Main')
  })
})
