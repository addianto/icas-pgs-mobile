'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Dimensions,
  Image,
  StyleSheet,
  TouchableHighlight
} from 'react-native'

export default class SplashScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)

    this.state = {
      delay: setTimeout(() => {
        this.props.navigation.navigate('Main')
      }, 3000)
    }
  }

  render () {
    return (
      <TouchableHighlight
        onPress={() => {
          clearTimeout(this.state.delay)
          if (!this.state.fontLoading) {
            this.props.navigation.navigate('Main')
          }
        }}
        style={styles.container}>
        <Image
          resizeMode='cover'
          style={{
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width
          }}
          source={require('../../assets/splash.jpg')} />
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  }
})
