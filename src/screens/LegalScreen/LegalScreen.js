'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  FlatList
} from 'react-native'
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Icon,
  Left,
  Title
} from 'native-base'
import LegalCard from './LegalCard'
import LegalNotice from '../../assets/legal.json'
import { templates } from '../../styles'

export default class LegalScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack } = this.props.navigation

    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Legal Notice</Title>
          </Body>
        </Header>
        <Content>
          <FlatList
            data={LegalNotice}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem} />
        </Content>
      </Container>
    )
  }

  _renderItem = ({item}) => (
    <LegalCard
      sectionTitle={item.title}
      content={item.content} />
  )

  _keyExtractor = (item, index) => item.title
}
