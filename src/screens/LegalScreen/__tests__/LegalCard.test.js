'use strict'
import 'react-native'
import React from 'react'
import LegalCard from '../LegalCard'
import renderer from 'react-test-renderer'

describe('a legal notice card', () => {
  it('renders correctly with all required props', () => {
    const rendered = renderer.create(
      <LegalCard
        sectionTitle='Legal Stuff'
        content="Please don't sue me" />
    ).toJSON()

    expect(rendered).toBeTruthy()
    expect(rendered).toMatchSnapshot()
  })
})
