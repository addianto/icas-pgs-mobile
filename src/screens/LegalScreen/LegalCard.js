'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import {
  Body,
  Card,
  CardItem,
  Left,
  Text
} from 'native-base'

export default class LegalCard extends Component {
  static propTypes = {
    sectionTitle: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired
  }

  render () {
    const { sectionTitle, content } = this.props

    return (
      <Card>
        <CardItem header>
          <Left>
            <Body>
              <Text style={styles.title}>{sectionTitle}</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem>
          <Body>
            <Text style={styles.content}>{content}</Text>
          </Body>
        </CardItem>
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    textAlign: 'justify'
  },
  title: {
    fontWeight: 'bold'
  }
})
