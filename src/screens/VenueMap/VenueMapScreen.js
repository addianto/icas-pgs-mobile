'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Dimensions,
  Image
} from 'react-native'
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Icon,
  Left,
  Title
} from 'native-base'
import { templates } from '../../styles'

export default class VenueMapScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack } = this.props.navigation
    const { height, width } = Dimensions.get('window')

    return (
      <Container>
        <Header style={templates.header}>
          <Left>
            <Button transparent onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Venue Map</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={{height: height, width: width}}
            resizeMode='contain'
            source={require('../../assets/map_full.png')} />
        </Content>
      </Container>
    )
  }
}
