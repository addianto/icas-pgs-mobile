'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  Content,
  List,
  Tab,
  Text
} from 'native-base'
import SessionListItem from './SessionListItem'

export default class StreamTab extends Component {
  static propTypes = {
    heading: PropTypes.string.isRequired,
    sessions: PropTypes.arrayOf(PropTypes.object).isRequired,
    title: PropTypes.string.isRequired
  }

  render () {
    const { heading, sessions, title } = this.props

    return (
      <Tab heading={heading}>
        <Content>
          <Text style={styles.title}>{title}</Text>
          <List
            dataArray={sessions}
            renderRow={this._renderSessionListItem} />
        </Content>
      </Tab>
    )
  }

  _renderSessionListItem (session) {
    return (
      <SessionListItem
        id={session.id}
        title={session.session}
        day={session.date}
        startTime={session.startTime}
        endTime={session.endTime}
        onPress={session.navigate} />
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontStyle: 'italic',
    fontSize: 16,
    justifyContent: 'center',
    marginVertical: 5,
    textAlign: 'center'
  }
})
