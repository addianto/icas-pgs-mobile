'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  Button,
  Icon,
  Left,
  Right,
  Text
} from 'native-base'
import AlternateColorListItem from '../../components/AlternateColorListItem'

export default class SessionListItem extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    day: PropTypes.number.isRequired,
    startTime: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
    onPress: PropTypes.func
  }

  render () {
    const { id, title, day, startTime, endTime, onPress } = this.props

    return (
      <AlternateColorListItem id={id}>
        <Left style={styles.leftSection}>
          <Text style={styles.title}>{title}</Text>
          <Text note style={styles.time}>Day {day} : {startTime} - {endTime}</Text>
        </Left>
        <Right style={styles.rightSection}>
          { onPress ? (
            <Button small transparent onPress={onPress}>
              <Icon name='paper' />
            </Button>
          ) : (
            <Button small transparent>
              <Icon name='paper' />
            </Button>
          ) }
        </Right>
      </AlternateColorListItem>
    )
  }
}

const styles = StyleSheet.create({
  leftSection: {
    flex: 2,
    flexDirection: 'column'
  },
  title: {
    alignSelf: 'flex-start'
  },
  time: {
    alignSelf: 'flex-start'
  },
  rightSection: {
    flex: 1
  }
})
