'use strict'
import 'react-native'
import React from 'react'
import StreamTab from '../StreamTab'
import renderer from 'react-test-renderer'

describe('a stream tab', () => {
  it('renders correctly', () => {
    let component = renderer.create(
      <StreamTab
        heading="Example Tab 1"
        title="Example Tab 1 Title"
        sessions={[
          {
            id: 1,
            session: 'Parallel Session 1',
            date: 1,
            startTime: '10:00',
            endTime: '10:30'
          },
          {
            id: 2,
            session: 'Parallel Session 2',
            date: 1,
            startTime: '10:30',
            endTime: '11:00'
          }
        ]} />
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
})
