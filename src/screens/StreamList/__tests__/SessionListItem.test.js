'use strict'
import React from 'react'
import SessionListItem from '../SessionListItem'
import renderer from 'react-test-renderer'

describe('a session list item', () => {
  it('renders correctly with required props', () => {
    const component = renderer.create(
      <SessionListItem
        id={1}
        title='Parallel Session 1'
        day={1}
        startTime='10:00'
        endTime='11:00' />
    )
    expect(component.toJSON()).toMatchSnapshot()
  })

  it('renders correctly with required & optional props', () => {
    const mockOnPress = jest.fn()
    const component = renderer.create(
      <SessionListItem
        id={1}
        title='Parallel Session 1'
        day={1}
        startTime='10:00'
        endTime='11:00'
        onPress={mockOnPress} />
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
})
