'use strict'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Linking
} from 'react-native'
import {
  Body,
  Button,
  Container,
  Header,
  Icon,
  Left,
  Right,
  Tabs,
  Title
} from 'native-base'
import StreamTab from './StreamTab'
import PaperData from '../../assets/papers.json'
import { getParallelSessions } from '../../util'
import { templates } from '../../styles'

export default class StreamListScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  render () {
    const { goBack, navigate } = this.props.navigation

    return (
      <Container>
        <Header hasTabs style={templates.header}>
          <Left>
            <Button
              transparent
              onPress={() => goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Parallel Sessions</Title>
          </Body>
          <Right>
            <Button iconRight transparent
              onPress={() => this._downloadParallelSessions()}>
              <Icon name='download' />
            </Button>
          </Right>
        </Header>
        <Tabs initialPage={0}>
          <StreamTab
            heading='Stream A'
            title='Public Sector Reform, Policy, and Innovation in Changing State-Market-Society Relations'
            sessions={this._getSessions('A', navigate)} />
          <StreamTab
            heading='Stream B'
            title='Strategic Governance and Organizational Readiness toward Regional Cooperation'
            sessions={this._getSessions('B', navigate)} />
          <StreamTab
            heading='Stream C'
            title='Promoting Adaptable Fiscal Policy towards Sustainable Development in Dynamic Public-Private-Society Relation'
            sessions={this._getSessions('C', navigate)} />
        </Tabs>
      </Container>
    )
  }

  _getSessions (initial, navigate) {
    const sessions = getParallelSessions(PaperData)
    const filteredSessions = sessions.filter(session => session['session'].startsWith(initial))

    filteredSessions.forEach((session, index) => {
      session['id'] = index + 1
      session['navigate'] = () => navigate('Papers', { parallelSession: session['session'] })
    })

    return filteredSessions
  }

  _downloadParallelSessions () {
    const url = 'https://icas-pgs.ui.ac.id/wp-content/uploads/sites/40/2017/10/Parallel-Session-Updated.pdf'

    console.log(`[Debug] Param URL: ${url}`)

    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log(`Can't handle url: ${url}`)
      } else {
        return Linking.openURL(url)
      }
    }).catch(err => console.error('An error occured', err))
  }
}
