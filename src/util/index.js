function normalizeAuthors (authorsString) {
  const authorsNames = authorsString.trim().split(',')

  authorsNames.forEach((authorName, index) => {
    let nameComponents = authorName.trim().split(' ')

    nameComponents.forEach((nameComponent, index) => {
      const upperCasedFirstLetter = nameComponent.charAt(0).toUpperCase()
      const updatedNameComponent = (nameComponent.length > 1)
        ? upperCasedFirstLetter + nameComponent.substring(1)
        : upperCasedFirstLetter
      nameComponents[index] = updatedNameComponent
    })

    if (nameComponents.length > 1 &&
      nameComponents.every(_isHavingSameNameComponents)) {
      nameComponents = nameComponents.splice(0, 1)
    }

    authorsNames[index] = nameComponents.reduce((accumulator, currentValue) => {
      return accumulator + ' ' + currentValue
    })
  })

  return authorsNames.reduce((accumulator, currentValue) => {
    return accumulator + ', ' + currentValue
  })
}

function _isHavingSameNameComponents (element, index, array) {
  return element === array[0]
}

function getPapersByParallelSession (papers, parallelSession) {
  const filteredPapers = []

  papers.filter(paper => paper['Parallel-Session'].trim() === parallelSession)
    .forEach(paper => {
      const time = paper['Time'].split('-')
      let date = paper['Date'].trim()
      date = Number(date.charAt(date.length - 1))

      const filteredPaper = {
        session: paper['Parallel-Session'].trim(),
        date: date,
        startTime: time[0].trim(),
        endTime: time[1].trim(),
        authors: normalizeAuthors(paper['Author'].trim()),
        abstract: paper['Abstract'].trim(),
        title: paper['Title'].trim(),
        room: paper['Room'].trim(),
        downloadUrl: paper['URL'].trim()
      }

      filteredPapers.push(filteredPaper)
    })

  let id = 0

  filteredPapers.forEach(paper => {
    paper['id'] = ++id
  })

  return filteredPapers
}

function getParallelSessions (papers) {
  const uniqueSessions = new Map()
  const parallelSessions = []

  papers.forEach(paper => {
    const time = paper['Time'].split('-')
    let date = paper['Date'].trim()
    date = Number(date.charAt(date.length - 1))

    const session = {
      stream: paper['Main-Stream'].trim(),
      session: paper['Parallel-Session'].trim(),
      date: date,
      startTime: time[0].trim(),
      endTime: time[1].trim()
    }

    if (!uniqueSessions.has(session['session'])) {
      uniqueSessions.set(session['session'], false)
    }

    if (uniqueSessions.get(session['session']) === false) {
      parallelSessions.push(session)
      uniqueSessions.set(session['session'], true)
    }
  })

  let id = 0

  parallelSessions.forEach(session => {
    session['id'] = ++id
  })

  return parallelSessions
}

export {
  getPapersByParallelSession,
  getParallelSessions,
  normalizeAuthors
}
