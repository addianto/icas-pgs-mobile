'use strict'
import {
  getPapersByParallelSession,
  getParallelSessions,
  normalizeAuthors
} from './index'

describe('getPapersByParallelSession', () => {
  const sampleData1 = [
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.1. Parallel Session 1',
      'Title': 'Title 1',
      'Author': 'Author 1',
      'Abstract': 'Abstract 1',
      'Room': 'Room A',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.1. Parallel Session 1',
      'Title': 'Title 2',
      'Author': 'Author 1, Author 2',
      'Abstract': 'Abstract 2',
      'Room': 'Room A',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.2. Parallel Session 1',
      'Title': 'Title 3',
      'Author': 'Author 1, Author 2, Author 3',
      'Abstract': 'Abstract 3',
      'Room': 'Room B',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.3. Parallel Session 1',
      'Title': 'Title 4',
      'Author': 'Author 5, Author 6',
      'Abstract': 'Abstract 4',
      'Room': 'Room C',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.3. Parallel Session 1',
      'Title': 'Title 5',
      'Author': 'Author 6',
      'Abstract': 'Abstract 5',
      'Room': 'Room C',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    }
  ]

  it('returns a new array of objects containing papers', () => {
    const result = getPapersByParallelSession(sampleData1, 'A.1. Parallel Session 1')
    expect(result.length).toBe(2)
    result.forEach(element => {
      expect(element.session).toBe('A.1. Parallel Session 1')
    })
  })

  it('reduces original data into papers-only data', () => {
    const result = getPapersByParallelSession(sampleData1, 'A.1. Parallel Session 1')
    result.forEach(element => {
      expect(element).toHaveProperty('id')
      expect(element).toHaveProperty('session')
      expect(element).toHaveProperty('date')
      expect(element).toHaveProperty('startTime')
      expect(element).toHaveProperty('endTime')
      expect(element).toHaveProperty('authors')
      expect(element).toHaveProperty('abstract')
      expect(element).toHaveProperty('title')
      expect(element).toHaveProperty('room')
      expect(element).toHaveProperty('downloadUrl')
    })
  })
})

describe('getParallelSessions function', () => {
  const sampleData1 = [
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.1. Parallel Session 1',
      'Title': 'Title 1',
      'Author': 'Author 1',
      'Abstract': 'Abstract 1',
      'Room': 'Room A',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.1. Parallel Session 1',
      'Title': 'Title 2',
      'Author': 'Author 1, Author 2',
      'Abstract': 'Abstract 2',
      'Room': 'Room A',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.2. Parallel Session 1',
      'Title': 'Title 3',
      'Author': 'Author 1, Author 2, Author 3',
      'Abstract': 'Abstract 3',
      'Room': 'Room B',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.3. Parallel Session 1',
      'Title': 'Title 4',
      'Author': 'Author 5, Author 6',
      'Abstract': 'Abstract 4',
      'Room': 'Room C',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    },
    {
      'Main-Stream': 'A. Public sector reform, policy, and innovation in changing state-market-society relations',
      'Parallel-Session': 'A.3. Parallel Session 1',
      'Title': 'Title 5',
      'Author': 'Author 6',
      'Abstract': 'Abstract 5',
      'Room': 'Room C',
      'Date': 'Day 1',
      'Time': '13:15 - 15:15 ',
      'URL': ''
    }
  ]

  it('returns a new array of objects containing parallel session schedules ', () => {
    const result = getParallelSessions(sampleData1)
    expect(result).not.toBe(sampleData1)
  })

  it('reduces original data into parallel session schedules', () => {
    const result = getParallelSessions(sampleData1)
    result.forEach(element => {
      expect(element).toHaveProperty('id')
      expect(element).toHaveProperty('stream')
      expect(element).toHaveProperty('session')
      expect(element).toHaveProperty('date')
      expect(element).toHaveProperty('startTime')
      expect(element).toHaveProperty('endTime')
    })
  })

  it('returns unique parallel session schedules (i.e. no dups)', () => {
    const result = getParallelSessions(sampleData1)
    expect(result.length).toBe(3)
  })
})

describe('normalizeAuthors function', () => {
  it('handles single duplicate first name cases', () => {
    let testCase = 'Lorem Lorem'
    expect(normalizeAuthors(testCase)).toBe('Lorem')
  })

  it('handles multiple duplicate first name cases', () => {
    let testCase = 'Lorem Lorem, Ipsum Ipsum, Dolor Dolor, Sit Sit'
    expect(normalizeAuthors(testCase)).toBe('Lorem, Ipsum, Dolor, Sit')
  })

  it('handles multiple duplicate first name cases mixed with normal names', () => {
    let testCase = 'Lorem Lorem, Ipsum Dolor, Sit Sit, Amet Amit'
    expect(normalizeAuthors(testCase)).toBe('Lorem, Ipsum Dolor, Sit, Amet Amit')
  })

  it('capitalizes first letter in every words', () => {
    let testCase = 'Lorem ipsum, dolor Sit, amet amit'
    expect(normalizeAuthors(testCase)).toBe('Lorem Ipsum, Dolor Sit, Amet Amit')
  })

  it('handles single correct author string', () => {
    let testCase = 'Lorem Ipsum'
    expect(normalizeAuthors(testCase)).toBe(testCase)
  })

  it('handles multiple correct author strings', () => {
    let testCase = 'Lorem Ipsum, Dolor Sit, Amet Amit'
    expect(normalizeAuthors(testCase)).toBe(testCase)
  })

  it('handles abbreviations in author name', () => {
    let testCase = 'Lorem i, Dolor S, A amit'
    expect(normalizeAuthors(testCase)).toBe('Lorem I, Dolor S, A Amit')
  })
})
