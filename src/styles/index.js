'use strict'
import {
  StyleSheet
} from 'react-native'

const colors = {
  'indigo': '#4b0082',
  'lavender': '#e6e6fa',
  'white': '#ffffff'
}

const templates = StyleSheet.create({
  header: {
    backgroundColor: colors.indigo
  }
})

export { colors, templates }
