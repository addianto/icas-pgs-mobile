# ICASPGS Mobile App

[![pipeline status](https://gitlab.com/addianto/icas-pgs-mobile/badges/master/pipeline.svg)](https://gitlab.com/addianto/icas-pgs-mobile/commits/master)
[![coverage report](https://gitlab.com/addianto/icas-pgs-mobile/badges/master/coverage.svg)](https://gitlab.com/addianto/icas-pgs-mobile/commits/master)

This is the project repository for [ICASPGS](https://icas-pgs.ui.ac.id)
(International Conference on Administrative Science, Policy, and Governance
Studies) mobile app. It is an Android-based mobile app developed using
[React Native](https://facebook.github.io/react-native/).

The project was bootstrapped with [CRNA (Create React Native App)](https://github.com/react-community/create-react-native-app).
The latest documentation related to CRNA is available [here](https://github.com/react-community/create-react-native-app/blob/master/react-native-scripts/template/README.md).

## License

[Apache 2.0](LICENSE) (c) Daya Adianto
