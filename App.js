'use strict'
import { StackNavigator } from 'react-navigation'
import ContactScreen from './src/screens/ContactScreen'
import LegalScreen from './src/screens/LegalScreen'
import MainMenu from './src/screens/MainMenu'
import NewsDetail from './src/screens/NewsDetail'
import NewsList from './src/screens/NewsList'
import PaperDetail from './src/screens/PaperDetail'
import PaperList from './src/screens/PaperList'
import PlenaryListScreen from './src/screens/PlenaryList'
import ScheduleList from './src/screens/ScheduleList'
import SplashScreen from './src/screens/SplashScreen'
import StreamList from './src/screens/StreamList'
import VenueMapScreen from './src/screens/VenueMap'

const App = StackNavigator({
  Contact: { screen: ContactScreen },
  Legal: { screen: LegalScreen },
  Main: { screen: MainMenu },
  News: { screen: NewsList },
  NewsContent: { screen: NewsDetail },
  Papers: { screen: PaperList },
  Paper: { screen: PaperDetail },
  Plenary: { screen: PlenaryListScreen },
  Splash: { screen: SplashScreen },
  Streams: { screen: StreamList },
  Schedules: { screen: ScheduleList },
  VenueMap: { screen: VenueMapScreen }
}, {
  headerMode: 'none',
  initialRouteName: 'Splash'
})

export default App
