/**
 * Wrapper for accessing papers & speakers data from their JSON objects.
 */
'use strict'

export default class Data {
  constructor (jsonData) {
    this.jsonData = jsonData
  }

  getJsonData () {
    return this.jsonData
  }

  filterByAttribute (attribute, query) {
    let result = this.jsonData.filter(obj => obj[attribute] === query)

    return result
  }

  transformToSections (attribute) {
    let sections = []

    this.jsonData.forEach(item => {
      let section = sections.find(element => {
        return item[attribute] === element[attribute]
      })

      if (!section) {
        section = {}
        section[attribute] = item[attribute]
        section['data'] = []
        sections.push(section)
      }

      section.data.push(item)
    })

    sections.sort((a, b) => {
      if (a[attribute] < b[attribute]) {
        return -1
      } else if (a[attribute] > b[attribute]) {
        return 1
      } else {
        return 0
      }
    })

    return sections
  }
}
