/**
 * Tests for Data module.
 */
'use strict'
import Data from '../Data'

describe('a Data object', () => {
  it('instantiates successfully', () => {
    expect(new Data()).toBeInstanceOf(Data)
  })

  it('returns given JSON data', () => {
    let jsonData = {
      'lorem': 'ipsum',
      'dolor': 'sit',
      'amet': 123,
      '456': true
    }
    let topicDataObj = new Data(jsonData)

    expect(topicDataObj.getJsonData()).toBe(jsonData)
  })

  it('stores list of objects from given JSON data', () => {
    let jsonList = [
      { 'lorem': 'ipsum' },
      { 'dolor': 'sit' },
      { 'amet': 42 }
    ]
    let topicDataObj = new Data(jsonList)

    expect(topicDataObj.getJsonData()).toBe(jsonList)
  })

  it('filters JSON objects by an attribute', () => {
    let jsonList = [
      {
        'id': 1,
        'name': 'Lorem Ipsum',
        'type': 'Cold'
      },
      {
        'id': 2,
        'name': 'Dolor Sit',
        'type': 'Cold'
      },
      {
        'id': 3,
        'name': 'Amet',
        'type': 'Hot'
      }
    ]
    let data = new Data(jsonList)

    expect(data.filterByAttribute('type', 'Cold')).toHaveLength(2)
  })

  it('prepares data into sections determined by an attribute', () => {
    let jsonList = [
      {
        'id': 1,
        'name': 'Lorem Ipsum',
        'type': 'Cold'
      },
      {
        'id': 2,
        'name': 'Dolor Sit',
        'type': 'Cold'
      },
      {
        'id': 3,
        'name': 'Amet',
        'type': 'Hot'
      }
    ]
    let data = new Data(jsonList)
    let sections = data.transformToSections('type')

    expect(sections[0].type).toBe('Cold')
    expect(sections[0].data).toHaveLength(2)
    expect(sections[1].type).toBe('Hot')
    expect(sections[1].data).toHaveLength(1)
  })

  it('prepares data into sections and sorted correctly', () => {
    let jsonList = [
      {
        'id': 1,
        'name': 'Lorem Ipsum',
        'type': 'Hot'
      },
      {
        'id': 2,
        'name': 'Dolor Sit',
        'type': 'Hot'
      },
      {
        'id': 3,
        'name': 'Amet',
        'type': 'Cold'
      }
    ]
    let data = new Data(jsonList)
    let sections = data.transformToSections('type')

    expect(sections[0].type).toBe('Cold')
    expect(sections[0].data).toHaveLength(1)
    expect(sections[1].type).toBe('Hot')
    expect(sections[1].data).toHaveLength(2)
  })
})
