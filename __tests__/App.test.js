'use strict'
import 'react-native'
import React from 'react'
import App from '../App'
import renderer from 'react-test-renderer'

// Source: https://github.com/react-community/react-navigation/issues/1599#issuecomment-308268444
jest.mock('Linking', () => {
  // We need to mock both Linking.getInitialURL()
  // and Linking.getInitialURL().then()
  const getInitialURL = jest.fn()
  getInitialURL.mockReturnValueOnce({then: jest.fn()})

  return {
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    openURL: jest.fn(),
    canOpenURL: jest.fn(),
    getInitialURL: getInitialURL
  }
})

describe('the app', () => {
  it('renders without crashing', () => {
    const rendered = renderer.create(<App />).toJSON()

    expect(rendered).toBeTruthy()
    expect(rendered).toMatchSnapshot()
  })
})
